class Chicken:
    """
    Класс Chicken.

    Поля:
    имя: name,
    номер загона: corral_num,
    сколько яиц в день: eggs_per_day.

    Методы:
    get_sound: вернуть строку 'Ко-ко-ко',
    get_info: вернуть строку вида:
    "Имя курицы: name
     Номер загона: corral_num
     Количество яиц в день: eggs_per_day"
    __lt__: вернуть результат сравнения количества яиц (<)
    """

    def __init__(self, n, c, e):
        self.name = n
        self.corral_num = c
        self.eggs_per_day = e

    def get_sound(self):
        return 'Ко-ко-ко'

    def get_info(self):
        return f"Имя курицы: {self.name}\n" \
               f"Номер загона: {self.corral_num}\n" \
               f"Количество яиц в день: {self.eggs_per_day}"

    def __lt__(self, other):
        return self.eggs_per_day < other.eggs_per_day
