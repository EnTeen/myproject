from topic_7_oop.hw.class_4_1_pupil import Pupil
from topic_7_oop.hw.class_4_2_worker import Worker


class School:
    """
    Класс School.

    Поля:
    список людей в школе (общий list для Pupil и Worker): people,
    номер школы: number.

    Методы:
    get_avg_mark: вернуть средний балл всех учеников школы
    get_avg_salary: вернуть среднюю зп работников школы
    get_worker_count: вернуть сколько всего работников в школе
    get_pupil_count: вернуть сколько всего учеников в школе
    get_pupil_names: вернуть все имена учеников (с повторами, если есть)
    get_unique_worker_positions: вернуть список всех должностей работников (без повторов, подсказка: set)
    get_max_pupil_age: вернуть возраст самого старшего ученика
    get_min_worker_salary: вернуть самую маленькую зп работника
    get_min_salary_worker_names: вернуть список имен работников с самой маленькой зп
    (список из одного или нескольких элементов)
    """

    def __init__(self, p, n):
        self.people = p
        self.number = n

    def get_avg_mark(self):  # вернуть средний балл всех учеников школы
        marks = [val.get_avg_mark() for val in self.people if isinstance(val, Pupil)]
        return sum(marks) / len(marks)

    def get_avg_salary(self):  # вернуть среднюю зп работников школы
        salary = [val.salary for val in self.people if isinstance(val, Worker)]
        return sum(salary) / len(salary)

    def get_worker_count(self):  # вернуть сколько всего работников в школе
        return len([val.name for val in self.people if isinstance(val, Worker)])

    def get_pupil_count(self):  # вернуть сколько всего учеников в школе
        return len([val.name for val in self.people if isinstance(val, Pupil)])

    def get_pupil_names(self):  # вернуть все имена учеников (с повторами, если есть)
        return [val.name for val in self.people if isinstance(val, Pupil)]

    def get_unique_worker_positions(self):  # вернуть список всех должностей работников (без повторов, подсказка: set)
        return set([val.position for val in self.people if isinstance(val, Worker)])

    def get_max_pupil_age(self):  # вернуть возраст самого старшего ученика
        return max([val.age for val in self.people if isinstance(val, Pupil)])

    def get_min_worker_salary(self):  # вернуть самую маленькую зп работника
        return min([val.salary for val in self.people if isinstance(val, Worker)])

    def get_min_salary_worker_names(
            self):  # вернуть список имен работников с самой маленькой зп (список из одного или нескольких элементов)
        return [val.name for val in self.people if
                isinstance(val, Worker) and val.salary == self.get_min_worker_salary()]