class Worker:
    """
    Класс Worker.

    Поля:
    имя: name,
    зарплата: salary,
    должность: position.

    Методы:
    __gt__: возвращает результат сравнения (>) зарплат работников.
    __len__: возвращает количетсво букв в названии должности.
    """

    def __init__(self, n, s, p):
        self.name = n
        self.salary = s
        self.position = p

    def __gt__(self, other):
        return self.salary > other.salary

    def __len__(self):
        return len(self.position)