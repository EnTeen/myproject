class Pupil:
    """
    Класс Pupil.

    Поля:
    имя: name,
    возраст: age,
    dict с оценками: marks (пример: {'math': [3, 5], 'english': [5, 5, 4] ...].

    Методы:
    get_all_marks: получить список всех оценок,
    get_avg_mark_by_subject: получить средний балл по предмету (если предмета не существует, то вернуть 0),
    get_avg_mark: получить средний балл (все предметы),
    __le__: вернуть результат сравнения (<=) средних баллов (все предметы) двух учеников.
    """

    def __init__(self, n, a, m: dict):
        self.name = n
        self.age = a
        self.marks = m

    def get_all_marks(self):
        marks = []
        for mark in self.marks.values():
            marks.extend(mark)
        return marks
    # return [m for mark in self.marks.values() for m in mark] так лучше

    def get_avg_mark_by_subject(self, subject):
        if self.marks.get(subject):
            return sum(self.marks[subject]) / len(self.marks[subject])
        else:
            return 0


    def get_avg_mark(self):
        marks = self.get_all_marks()
        return sum(marks) / len(marks)

    def __le__(self, other):
        return self.get_avg_mark() <= other.get_avg_mark()

if __name__ == '__main__':
    aaa = Pupil ("aaaa", 23, {
                           'math': [3, 3, 3],
                           'history': [3, 5, 3],
                           'english': [3, 3, 3]
                       })

    print(aaa.get_all_marks())
    print(aaa.get_avg_mark_by_subject("history"))
    print(aaa.get_avg_mark())
