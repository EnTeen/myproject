class Goat:
    """
    Класс Goat.

    Поля:
    имя: name,
    возраст: age,
    сколько молока дает в день: milk_per_day.

    Методы:
    get_sound: вернуть строку 'Бе-бе-бе',
    __invert__: реверс строки с именем (например, была Маруся, а стала Ясурам). вернуть self
    __mul__: умножить milk_per_day на число. вернуть self
    """

    def __init__(self, n, a, m):
        self.name = n
        self.age = a
        self.milk_per_day = m

    def get_sound(self):
        return 'Бе-бе-бе'

    def __invert__(self):
        self.name = self.name[::-1].lower().capitalize()
        return self

    def __mul__(self, other):
        self.milk_per_day = self.milk_per_day * other
        return self

if __name__ == '__main__':
    goat = Goat("Qwerty", 23, 2)
    print(goat.get_sound())
    print(~goat)
#   print(goat.__invert__()) не принято так
    print(goat * 3)
#   print(goat.__mul__(3)) не принято так
    print(goat.name)