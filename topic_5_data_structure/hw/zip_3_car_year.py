from itertools import zip_longest


def zip_car_year(car: list, year: list):
    """
    Функция zip_car_year.

    Принимает 2 аргумента: список с машинами и список с годами производства.

    Возвращает список с парами значений из каждого аргумента, если один список больше другого,
    то заполнить недостающие элементы строкой "???".

    Подсказка: zip_longest.

    Если вместо списков передано что-то другое, то возвращать строку 'Must be list!'.
    Если список (хотя бы один) пуст, то возвращать строку 'Empty list!'.
    """

    if not isinstance(car, list):
        return 'Must be list!'
    if not isinstance(year, list):
        return 'Must be list!'

    if not (car and year):
        return 'Empty list!'

    return list(zip_longest(car, year, fillvalue='???'))
