def zip_names(my_list: list, last_names: set):
    """
    Функция zip_names.

    Принимает 2 аргумента: список с именами и множество с фамилиями.

    Возвращает список с парами значений из каждого аргумента.

    Если вместо list передано что-то другое, то возвращать строку 'First arg must be list!'.
    Если вместо set передано что-то другое, то возвращать строку 'Second arg must be set!'.

    Если list пуст, то возвращать строку 'Empty list!'.
    Если set пуст, то возвращать строку 'Empty set!'.

    Если list и set различного размера, обрезаем до минимального (стандартный zip).
    """

    if not isinstance(my_list, list):
        return 'First arg must be list!'
    if not isinstance(last_names, set):
        return 'Second arg must be set!'

    if len(my_list) == 0:
        return 'Empty list!'
    if len(last_names) == 0:
        return 'Empty set!'

    return list(zip(my_list, last_names))


if __name__ == '__main__':
    my_list = ['Rose']
    my_set = {'Black', 'Pink'}

    print(zip_names(my_list, my_set))
