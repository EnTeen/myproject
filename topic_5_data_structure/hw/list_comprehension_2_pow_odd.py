def pow_odd(n):
    """
    Функция pow_odd.

    Принимает число n.

    Возвращает список, состоящий из квадратов нечетных чисел в диапазоне от 0 до n (не включая).

    Пример: n = 7, нечетные числа [1, 3, 5], результат [1, 9, 25]

    Если n не является int, то вернуть строку 'Must be int!'.
    """

    if not isinstance(n, int):
        return 'Must be int!'

    return [val ** 2 for val in range(n) if val % 2 != 0]
