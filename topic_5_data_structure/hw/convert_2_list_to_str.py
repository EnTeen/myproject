def list_to_str (my_list, my_separator):
    """
    Функция list_to_str.

    Принимает 2 аргумента: список и разделитель (строка).

    Возвращает (строку полученную разделением элементов списка разделителем,
    количество разделителей в получившейся строке в квадрате).

    Если вместо списка передано что-то другое, то возвращать строку 'First arg must be list!'.

    Если разделитель не является строкой, то возвращать строку 'Second arg must be str!'.

    Если список пуст, то возвращать пустой tuple().

    ВНИМАНИЕ: в списке могут быть элементы любого типа (нужно конвертировать в строку).
    """

    if not isinstance(my_list, list):
        return 'First arg must be list!'
    if not isinstance(my_separator, str):
        return 'Second arg must be str!'
    if len(my_list) == 0:
        return tuple(my_list)

    return (my_separator.join(map(str,my_list)), (len(my_list) - 1) ** 2)

if __name__ == '__main__':
    my_list = [111, 222, 333, 222]
    my_separator = "!"
    list_to_str(my_list, my_separator)