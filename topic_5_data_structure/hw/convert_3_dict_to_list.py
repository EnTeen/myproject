def dict_to_list (my_dict: dict):

    """
    Функция dict_to_list.

    Принимает 1 аргумент: словарь.

    Возвращает кортеж: (
    список ключей,
    список значений,
    количество уникальных элементов в списке ключей,
    количество уникальных элементов в списке значений
    ).

    Если вместо словаря передано что-то другое, то возвращать строку 'Must be dict!'.
    """

    if not isinstance(my_dict, dict):
        return 'Must be dict!'

    keys = list(my_dict.keys())
    len_keys = len(set(keys))
    values = list(my_dict.values())
    len_values = len(set(values))

    return (keys, values, len_keys, len_values)

if __name__ == '__main__':
    my_dict = {1: 'q', 2: 'w', 3: '3'}
    dict_to_list(my_dict)
