def get_words_by_translation (my_dict: dict, word: str):
    """
    Функция get_words_by_translation.

    Принимает 2 аргумента:
    ru-eng словарь содержащий {ru_word: [eng_1, eng_2 ...], ...}
    слово для поиска в словаре (eng).

    Возвращает список всех вариантов переводов (ru), если такое слово есть в словаре (eng),
    если нет, то ‘Can`t find English word: {word}’.

    Если вместо словаря передано что-то другое, то возвращать строку 'Dictionary must be dict!'.
    Если вместо строки для поиска передано что-то другое, то возвращать строку 'Word must be str!'.

    Если словарь пустой, то возвращать строку 'Dictionary is empty!'.
    Если строка для поиска пустая, то возвращать строку 'Word is empty!'.
    """

    if not isinstance(my_dict, dict):
        return 'Dictionary must be dict!'
    if not isinstance(word, str):
        return 'Word must be str!'

    if len(my_dict) == 0:
        return 'Dictionary is empty!'
    if len(word) == 0:
        return 'Word is empty!'

    my_list =[]
    for my_keys, my_val in my_dict.items():
        if word in my_val:
            my_list.append(my_keys)

    if my_list:
        return my_list
    else:
        return f"Can't find English word: {word}"


if __name__ == '__main__':
    my_dict = {"кот": ["cat"], "человек": ["man", "human", "person"]}
    print(get_words_by_translation(my_dict, "human"))