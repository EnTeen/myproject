import itertools


def pow5_start_inf_step(start, step):
    """
    Функция pow5_start_inf_step.

    Принимает 2 аргумента: число start, step.

    Возвращает генератор-выражение состоящий из
    значений в 5 степени от start до бесконечности с шагом step.

    Пример: start=3, step=2 результат 3^5, 5^5, 7^5, 9^5 ... (infinity).

    Если start или step не являются int, то вернуть строку 'Start and Step must be int!'.
    """

    if not isinstance(start, int) or not isinstance(step, int):
        return 'Start and Step must be int!'

    return (val ** 5 for val in itertools.count(start, step))
