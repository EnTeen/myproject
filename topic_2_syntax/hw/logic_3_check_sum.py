def check_sum(number_one, number_two, number_three):
    """
    Функция check_sum.

    Принимает 3 числа.
    Вернуть True, если можно взять какие-то два из них и в сумме получить третье, иначе False
    """

    ...

    if (number_one + number_two == number_three) or (number_one + number_three == number_two) or (
            number_three + number_two == number_one):
        return True
    else:
        return False
