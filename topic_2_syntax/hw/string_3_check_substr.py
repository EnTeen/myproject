def check_substr(str1, str2):
    """
    Функция check_substr.

    Принимает две строки.
    Если меньшая по длине строка содержится в большей, то возвращает True,
    иначе False.
    Если строки равны, то False.
    Если одна из строк пустая, то True.
    """

    ...

    if str1 == str2:
        return False
    elif str1 == "" or str2 == "":
        return True
    elif len(str1) < len(str2):
        return str2.find(str1) > -1
        # return True if str2.find(str1) != -1 else False
    elif len(str2) < len(str1):
        return str1.find(str2) > -1
        # return True if str1.find(str2) != -1 else False


if __name__ == '__main__':
    print(check_substr("", ""))
    print(check_substr("DDD", "DDD"))
    print(check_substr("DDD23", "23"))