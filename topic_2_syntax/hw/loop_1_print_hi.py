def print_hi(n):
    """
    Функция print_hi.

    Принимает число n.
    Выведите на экран n раз фразу "Hi, friend!"
    Пример: n=3, тогда в результате "Hi, friend!Hi, friend!Hi, friend!"
    """

    ...
    if n >= 0:
        print("Hi, friend!" * n)
    else:
        print("")


if __name__ == '__main__':
    print_hi(-3)
