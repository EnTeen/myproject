def print_symbols_if (string):
    """
    Функция print_symbols_if.

    Принимает строку.

    Если строка нулевой длины, то вывести строку "Empty string!".

    Если длина строки больше 5, то вывести первые три символа и последние три символа.
    Пример: string='123456789' => result='123789'

    Иначе вывести первый символ столько раз, какова длина строки.
    Пример: string='345' => result='333'
    """

    ...

    if string == "":
        print("Empty string!")
    elif len(string) > 5:
        print (string[:3] + string[-3:])
        # print(string[:3:1]+string[len(string)-3::1])
    else:
        print(string[0]*len(string))


if __name__ == '__main__':
    print_symbols_if("345")