def count_odd_num(number):
    """
    Функция count_odd_num.

    Принимает натуральное число (целое число > 0).
    Верните количество нечетных цифр в этом числе.
    Если число равно 0, то вернуть "Must be > 0!".
    Если число не целое (не int, а другой тип данных), то вернуть "Must be i".
    """

    ...

    if type(number) != int:
        return "Must be int!"
    elif number > 0:
        count = 0
        for i in str(number):
            if int(i) % 2 != 0:
                count += 1
        return count
    else:
        return "Must be > 0!"


if __name__ == '__main__':
    count_odd_num(123)