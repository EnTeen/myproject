def arithmetic(number_one, number_two, operation):
    """
    Функция arithmetic.

    Принимает 3 аргумента: первые 2 - числа, третий - операция, которая должна быть произведена над ними.
    Если третий аргумент +, сложить их;
    если —, то вычесть;
    если *, то умножить;
    если /, то разделить (первое на второе).
    В остальных случаях вернуть строку "Unknown operator".
    Вернуть результат операции.
    """

    ...

    if operation == "-":
        return number_one - number_two
    elif operation == "*":
        return number_one * number_two
    elif operation == "/":
        return number_one / number_two
    elif operation == "+":
        return number_one + number_two
    else:
        return "Unknown operator"


if __name__ == '__main__':
    arithmetic(2, 3, "*")
