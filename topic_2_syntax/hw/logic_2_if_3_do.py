def if_3_do(number):
    """
    Функция if_3_do.

    Принимает число.
    Если оно больше 3, то увеличить число на 10, иначе уменьшить на 10.
    Вернуть результат.
    """

    ...
    if type(number) == int or type(number) == float:
        return number + 10 if number > 3 else number - 10
