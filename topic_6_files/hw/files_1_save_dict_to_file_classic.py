def save_dict_to_file_classic (name_or_path, my_dict):
    """
    Функция save_dict_to_file_classic.

    Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

    Сохраняет список в файл. Проверить, что записалось в файл.
    """

    with open(name_or_path, "w") as file:
        file.write((str(my_dict)))

if __name__ == '__main__':
    my_file = 'task1.txt'
    my_dict = {'rrr': '3-5-33', 'tre': '083-7363-33', 3: 'три'}
    save_dict_to_file_classic(my_file, my_dict)