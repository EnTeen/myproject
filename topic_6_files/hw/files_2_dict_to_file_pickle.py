import pickle

def save_dict_to_file_pickle(name_or_path, my_dict):
    """
    Функция save_dict_to_file_pickle.

    Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

    Сохраняет список в файл. Загрузить словарь и проверить его на корректность.
    """

    with open(name_or_path, "wb") as my_file:
        pickle.dump(my_dict, my_file)

    with open(name_or_path, "rb") as my_file:
        my_load_dict = pickle.load(my_file)

    print(f"my_load_dict == my_dict: {my_load_dict == my_dict}")
    print(f"type(my_load_dict): {type(my_load_dict)}")


if __name__ == '__main__':
    name_or_path = "task_dict"
    my_dict = {'rrr': '3-5-33', 'tre': '083-7363-33', 3: 'три'}
    save_dict_to_file_pickle(name_or_path, my_dict)
