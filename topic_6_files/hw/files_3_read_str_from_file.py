def read_str_from_file(name_or_path):
    """
    Функция read_str_from_file.

    Принимает 1 аргумент: строка (название файла или полный путь к файлу).

    Выводит содержимое файла (в консоль). (* Файл должен быть предварительно создан и наполнен каким-то текстом.)
    """

    with open(name_or_path, "w") as file:
        file.write("Выводит содержимое файла (в консоль). (* Файл должен быть предварительно создан и наполнен "
                   "каким-то текстом.)")

    with open(name_or_path, "r") as file:
        print(file.read())


if __name__ == '__main__':
    my_file = 'task1.txt'
    read_str_from_file(my_file)
