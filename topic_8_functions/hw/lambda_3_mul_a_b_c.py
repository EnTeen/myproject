def mul_a_b_c(a, b, c):
    """
    lambda перемножает аргументы a, b и c и выводит результат
    """

    print((lambda x, y, z: x * y * z)(a, b, c))


if __name__ == '__main__':
    mul_a_b_c(1, 3, 6)
    mul_a_b_c(2, 2, 2)