def print_student_marks_by_subject(name: str, **kwargs):
    """
    Функция print_student_marks_by_subject.
    
    Принимает 2 аргумента: имя студента и именованные аргументы с оценками по различным предметам (**kwargs).
    
    Выводит на экран имя студента, а затем предмет и оценку (может быть несколько, если курс состоял из нескольких частей).
    
    Пример вызова функции print_student_marks_by_subject("Вася", math=5, biology=(3, 4), magic=(4, 5, 5)).
    """

    print(f"{name}")
    for sabj, marks in kwargs.items():
        print(sabj, ":", marks if isinstance(marks, int) else ", ".join([str(mark) for mark in marks]))

if __name__ == '__main__':
    print_student_marks_by_subject("Вася", math=5, biology=(3, 4), magic=(4, 5, 5))
