def pow_2(a):
    """
    lambda возводит в квадрат переданный аргумент возвращает результат
    """

    print((lambda x: x * x)(a))


if __name__ == '__main__':
    pow_2(2)
    pow_2(3)
