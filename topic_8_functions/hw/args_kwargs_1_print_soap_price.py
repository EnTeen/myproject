def print_soap_price(soap: str, *args):
    """
    Функция print_soap_price.

    Принимает 2 аргумента: название мыла (строка) и неопределенное количество цен (*args).

    Функция print_soap_price выводит вначале название мыла, а потом все цены на него.

    В функции main вызывается функция print_soap_price и передается название мяла и произвольное количество цен.

    Пример: print_soap_price(‘Dove’, 10, 50) или print_soap_price(‘Мылко’, 456, 876, 555).
    """

    temp = ""
    for price in args:
        temp = temp + ", " + str(price)

    print(f"‘{soap}’{temp}")

if __name__ == '__main__':
    print_soap_price("Dove", 10, 50)
    print_soap_price("Мылко", 456, 876, 555)
