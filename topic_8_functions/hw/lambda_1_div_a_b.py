def split(a, b):
    """
    lambda делит аргумент a на аргумент b и выводит результат
    """

    print((lambda x, y: x / y)(a, b))


if __name__ == '__main__':
    split(3, 5)
    split(100, 4)
