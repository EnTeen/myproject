def flower_with_vals(flowers=None, color=None, price=0):
    """
    Функция flower_with_vals.

    Принимает 3 аргумента:
    цветок (по умолчанию "ромашка"),
    цвет (по умолчанию "белый"),
    цена (по умолчанию 10.25).

    Функция flower_with_default_vals выводит строку в формате
    "Цветок: <цветок> | Цвет: <цвет> | Цена: <цена>".

    При этом в функции flower_with_default_vals есть проверка на то, что цветок и цвет - это строки

    (* Подсказка: type(x) == str или isinstance(s, str)), а также цена - это число больше 0, но меньше 10000.
    В функции main вызвать функцию flower_with_default_vals различными способами
    (перебрать варианты: цветок, цвет, цена, цветок цвет, цветок цена, цвет цена, цветок цвет цена).

    (* Использовать именованные аргументы).
    """
    if (type(flowers and color) == str) and 0 < price < 10000:
        print(f"Цветок: <{flowers}> | Цвет: <{color}> | Цена: <{price}>")
    else:
        print("Не верно указан цветок, цвет или цена")


if __name__ == '__main__':
    flower_with_vals("Роза")
    flower_with_vals(color="Желтый")
    flower_with_vals(price=434)
    flower_with_vals("Ромашка", "Белый")
    flower_with_vals("Орхидея", price=33)
    flower_with_vals(color="Желтый", price=434)
    flower_with_vals("Роза", "Синий", 8.30)
