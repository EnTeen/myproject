def summ_a_b_c(a, b, c):
    """
    lambda суммирует аргументы a, b и c и выводит результат
    """
    print((lambda x, y, z: x + y + z)(a, b, c))


if __name__ == '__main__':
    summ_a_b_c(1, 2, 3)
